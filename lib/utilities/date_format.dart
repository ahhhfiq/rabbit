extension ReadableDurationFormatter on Duration {
  String get readableShort {
    if (this.inDays >= 365) {
      return '${(this.inDays / 365).floor()}y';
    } else if (this.inDays >= 30) {
      return '${(this.inDays / 30).floor()}mo';
    } else if (this.inDays >= 1) {
      return '${(this.inDays)}d';
    } else if (this.inHours >= 1) {
      return '${(this.inHours)}h';
    } else if (this.inMinutes >= 1) {
      return '${(this.inMinutes)}m';
    } else {
      return '${(this.inSeconds)}s';
    }
  }
}
