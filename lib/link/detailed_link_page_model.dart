import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:simple_model_state/simple_model_state.dart';
import 'package:rednit/rednit.dart';

import 'detailed_link_page_state.dart';

// enum DetailedLinkPageState { loadingComments, hasComments }

class DetailedLinkPageModel extends BaseModel {
  final RedditClient _redditClient;
  final Link link;

  DetailedLinkPageState _state;
  DetailedLinkPageState get state => _state;

  DetailedLinkPageModel({
    @required this.link,
    @required RedditClient redditClient,
  })  : _redditClient = redditClient,
        _state = DetailedLinkPageState.loading(link: link) {
    _init();
  }

  Future<void> _init() async {
    final commentListing = await _redditClient.getComments(state.link.id);
    state.when(
      loading: (link) {
        _state = DetailedLinkPageState.success(
          link: state.link,
          comments: commentListing.children,
        );
      },
      success: (link, comments) {
        _state = DetailedLinkPageState.success(
          link: state.link,
          comments: [...comments, ...commentListing.children],
        );
      },
    );

    notifyListeners();
  }
}
