import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'splash_page_model.dart';
import '../home/home_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  SplashPageModel _model;

  @override
  void initState() {
    super.initState();
    _model = SplashPageModel(
      sharedPreferences: Provider.of(context, listen: false),
      redditClient: Provider.of(context, listen: false),
      onDone: () => Navigator.of(context).pushReplacement(
        MaterialPageRoute<void>(
          settings: RouteSettings(
            isInitialRoute: true,
          ),
          builder: (context) => HomePage(),
        ),
        // FadeThroughRoute<void>(
        //   widget: HomePage(),
        // ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashPageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          body: Center(
            child: Text('Hold on :P'),
          ),
        );
      },
    );
  }
}
